// Importamos los modulos de los cuales vamos a depender
const express = require('express');
const logger = require('morgan');
const errorhandler = require('errorhandler');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Accounts = require('./models/Accounts')

// Definimos la conexion a mongoDB
/*
const url = 'mongodb://192.168.31.5:27017/accounts'
*/
let app = express()

app.use(logger('dev'))
app.use(bodyParser.json())

// Conexion con mongoose

mongoose.connect('mongodb://192.168.31.5:27017/accounts', { useUnifiedTopology: true, useNewUrlParser: true });

mongoose.Promise = global.Promise;

var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)

db.on("error", console.error.bind('Error de conexión con MongoDB'));



// Visualizacion de las cuentas

app.get('/accounts', (req, res) => {
  Accounts.find({}, function (err, acc) {
    if (err) {
      res.status(500).send(err.message);
    }

    res.status(200).json({
      Cuentas: acc
    });


  })
})

app.post('/accounts', (req, res) => {
  let newAccount = req.body;
  Accounts.create(newAccount, function (err, newAccount) {
    if (err) {
      res.status(500).send(err.message);
    }

    res.status(200).json({
      Cuenta: newAccount
    })
  })
})


app.put('/accounts/:id', (req, res) => {
  let filtro = req.params.id
  Accounts.findByIdAndUpdate(filtro, req.body, function(err){
    if(err){
      res.status(500).send(err.message);
    }


    res.status(200).json({
      Actualizado : req.body
    })
  })
})


app.delete('/accounts/:id', (req, res) => {
  Accounts.remove({_id:req.params.id}, function(err, cuentaBorrada){
    if(err){
      res.status(500).send(err.message);
    }



    res.status(200).json({
      Borrado : cuentaBorrada
    })
  })      
})
app.listen(3000);