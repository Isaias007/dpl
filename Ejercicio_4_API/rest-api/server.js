//Aqui se realizan las importaciones
const express = require('express');
const logger = require('morgan');
const errorhandler = require('errorhandler');
const bodyParser = require('body-parser');

let store = {};
store.accounts = [];
//Aqui lanzamos express en la variable app
let app = express();
// Esto es lo que más me cuesta comprender no entiendo que cual es el objetivo.
app.use(bodyParser.json());
//Aqui usamos el middelware 
app.use(logger('dev'));
app.use(errorhandler());

app.get('/accounts', (req, res) => {
    res.status(200).send(store.accounts)
});

app.get('/accounts/:id', (req, res) => {
    res.status(200).send(store.accounts[req.params.id])
});

app.post('/accounts', (req, res) => {
    let newAccount = req.body
    let id = store.accounts.length
    store.accounts.push(newAccount)
    res.status(201).send({id: id})
});

app.put('/accounts/:id', (req, res) => {
    store.accounts[req.params.id] = req.body
    res.status(200).send(store.accounts[req.params.id])
});

app.delete('/accounts/:id', (req, res) => {
    store.accounts.splice(req.params.id, 1)
    res.status(204).send()
});


app.listen(3000);
console.log("Servidor corriendo en el puerto 3000");


/*
Basicamente la diferencia entre una variable var y una variable let es su localisación 
la variable var es una variable global el cual podemos utilizar facilmente en cualquier
parte del codigo y la variable let se necesitan diferentes "Propiedades" para poder utilizarlas
*/ 

/*
200: Este es el codigo http que quiere decir que todo a respondido sin problemas.

201: Este codigo indica que el cliente a recibido la peticion de creacion y a sido procesada correctamente

204: Este codigo indica que la solicitud a sido recibida correctamente pero no devuelve ningun contenido

*/ 


