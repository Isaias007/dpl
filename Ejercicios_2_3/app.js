var express = require('express');
var logger = require('morgan');
var dia = require('../Ejercicios_1/fecha.js');
var app = express();
app.use(logger('dev'));

app.get('/fecha', function(req, res) {
  res.send("La fecha del dia de hoy es :" + dia.fecha());
});

app.get('/about', function(req, res) {
  res.write("Isaias Barrera Monroy \n");
  res.write("Edad : 20 \n");
  res.write("Comida Favorita : Sushi ");
  res.end();
});

app.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});